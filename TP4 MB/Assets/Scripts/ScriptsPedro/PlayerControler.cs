﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControler : MonoBehaviour
{
    public float speed;

    public float fallSpeed;

    Rigidbody _rigidbody;

    Animator _animator;

    public static int powers;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _animator = GetComponent<Animator>();
    }

    void Start()
    {
        powers = 0;
    }

    
    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        

        _animator.SetFloat("Horizontal", horizontal);

        transform.Translate(new Vector3(horizontal * speed, 0, 0)*Time.deltaTime);

        if(horizontal < 0 || horizontal > 0)
        {
            _animator.SetBool("Idle", false);
        }
        else
        {
            _animator.SetBool("Idle", true);
        }

        if(powers == 2)
        {
            SpriteRenderer _sr = GetComponent<SpriteRenderer>();

            _sr.material.color = Color.red + Color.blue;

            StartCoroutine(RestartPower(3.5f));
        }
    }

    private void FixedUpdate()
    {
        if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space))
        {
            _rigidbody.AddForce(Vector2.up * fallSpeed, ForceMode.Impulse);
        }
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            _animator.SetTrigger("Fall");
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            _animator.SetBool("IsFalling", false);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            _animator.SetBool("IsFalling", true);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PowerUp"))
        {
            powers = Random.Range(1, 2);
            Destroy(other.gameObject);
        }
    }

    IEnumerator RestartPower(float pTime)
    {
        yield return new WaitForSecondsRealtime(pTime);
        powers = 0;
    }

}
