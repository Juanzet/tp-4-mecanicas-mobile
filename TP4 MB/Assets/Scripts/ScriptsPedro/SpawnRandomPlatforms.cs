﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRandomPlatforms : MonoBehaviour
{
    public List<GameObject> SpawnPoints;

    public List<GameObject> Platforms;

    bool spawn;

    public static int i;

    void Start()
    {
        
    }

    
    void Update()
    {
        for(i = 0; i < 1; i++)
        {
            StartCoroutine(SpawnRoutine(1.5f));
        }

        
    }

    IEnumerator SpawnRoutine(float pTime)
    {
        yield return new WaitForSecondsRealtime(pTime);

        Instantiate(Platforms[Random.Range(0, 2)], SpawnPoints[Random.Range(0, 3)].transform.position, Platforms[Random.Range(0, 2)].transform.rotation);
    }

}
