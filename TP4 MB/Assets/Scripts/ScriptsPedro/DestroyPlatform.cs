﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyPlatform : MonoBehaviour
{

    Animator _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    void Start()
    {
        SpawnRandomPlatforms.i = 0;
    }

    
    void Update()
    {
        
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            StartCoroutine(DestroyRoutine(.5f));
        }
    }


    IEnumerator DestroyRoutine(float pTime)
    {
        yield return new WaitForSecondsRealtime(pTime);
        _animator.SetBool("OnFloor", true);
        yield return new WaitForSecondsRealtime(pTime);
        Destroy(gameObject);
    }


}
