﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Image _menu;
    public Image _gameOver;
    public static bool pause;
    public static bool win;
    public Text points;
    public Text faster;
    public GameObject GameOverGUI;

    Animator _animator;

    string levelDecide; 
    int index;

    public static int stage;

    private void Awake()
    {
        _animator = _gameOver.GetComponent<Animator>();
    }

    void Start()
    {
        levelDecide = SceneManager.GetActiveScene().name;
        _menu.gameObject.SetActive(false);
        _gameOver.gameObject.SetActive(false);
        GameOverGUI.SetActive(false);
        faster.gameObject.SetActive(false);
        pause = false;
        win = false;
        ActualLevel(levelDecide);
        stage = 0;
    }

    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && pause == false)
        {
            _menu.gameObject.SetActive(true);
            pause = true;
        }else if(Input.GetKeyDown(KeyCode.Escape) && pause == true)
        {
            _menu.gameObject.SetActive(false);
            pause = false;
        }
        #region Mecanica13
        if (index == 2)
        {

            if(stage < 1) //Primer stage
            {
                points.text = "= " + SphereRotate.Points.ToString() + "/15";

                if (SphereRotate.Points == 15)
                {
                    StartCoroutine(TextRoutine(5.0f));
                    stage++;

                }
            }

            if(stage == 1) //Segundo stage
            {
                points.text = "= " + SphereRotate.Points.ToString() + "/25";

                if (SphereRotate.Points == 25)
                {
                    StartCoroutine(TextRoutine(5.0f));
                    stage++;

                }
            }

            if (stage == 2)//Tercer y ultimo stage
            {
                points.text = "= " + SphereRotate.Points.ToString() + "/30";

                if (SphereRotate.Points == 30)
                {
                    StartCoroutine(TextRoutine(1.2f));
                    stage++;
                    win = true;

                }
            }

            if (SphereRotate.gameOver == true)
            {
                for (int i = 0; i < 1; i++)
                {
                    StartCoroutine(FadeRoutine(2.0f));
                }
                pause = true;
            }


        }
        #endregion





    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void SelectLevel(int level)
    {
        switch (level)
        {
            case 1:
                SceneManager.LoadScene("Mecanica7");
                break;
            case 2:
                SceneManager.LoadScene("Mecanica13");
                break;
            case 3:
                SceneManager.LoadScene("Mecanica31");
                break;
            case 4:
                SceneManager.LoadScene("Mecanica34");
                break;
            case 5:
                SceneManager.LoadScene("MecanicaLibre");
                break;
            default:
                break;
        }

    }


    public void Quit()
    {
        Application.Quit();
    }

    private int ActualLevel(string lvlName)
    {
        switch (lvlName)
        {
            case "Mecanica7":
                index = 1;
                break;
            case "Mecanica13":
                index = 2;
                break;
            case "Mecanica31":
                index = 3;
                break;
            case "Mecanica34":
                index = 4;
                break;
            default:
                index = 0;
                break;
        }

        return index;
    }
    
    public void Retry()
    {
        SceneManager.LoadScene(levelDecide);
    }

    IEnumerator FadeRoutine(float pTime)
    {
        _gameOver.gameObject.SetActive(true);
        _animator.SetTrigger("Fade");

        yield return new WaitForSecondsRealtime(pTime);

        GameOverGUI.SetActive(true);
    }

    IEnumerator TextRoutine(float pTime)
    {
        Animator anim = faster.GetComponent<Animator>();
        faster.gameObject.SetActive(true);
        anim.SetTrigger("BounceIn");
        pause = true;
        yield return new WaitForSecondsRealtime(pTime);
        faster.gameObject.SetActive(false);
        pause = false;
    }

}
