﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeShoot : MonoBehaviour
{
    Rigidbody _rigidbody;

    public float throwSpeed;


    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    void Start()
    {
        
    }

    
    void FixedUpdate()
    {      
        
        _rigidbody.AddForce((Vector3.right * -1) * throwSpeed, ForceMode.Impulse);
   

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ball"))
        {
            _rigidbody.isKinematic = true;

            transform.SetParent(collision.gameObject.transform);
            SphereRotate.Points++;

        }

        if (collision.gameObject.CompareTag("Cube"))
        {
            SphereRotate.gameOver = true;
        }
    }

}
