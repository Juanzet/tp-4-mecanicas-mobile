﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingCube : MonoBehaviour
{
    public Camera _camera;

    MeshRenderer _meshRenderer;

    Rigidbody _rigidbody;

    bool floating;

    public int StartState;

    private void Awake()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
        _rigidbody = GetComponent<Rigidbody>();
        
    }

    void Start()
    {

        switch (StartState)
        {
            case 1:
                _rigidbody.useGravity = false;
                _rigidbody.AddForce(Vector3.up * 5f, ForceMode.Impulse);
                _meshRenderer.material.color = Color.blue;               
                break;
            case 2:
                _rigidbody.useGravity = true;
                _meshRenderer.material.color = Color.red + Color.yellow;
                break;
            default:
                break;
        }
        
    }

   
    void Update()
    {
        if(GameManager.pause == false)
        {
            FloatCube();
        }
        
    }


    private void FloatCube()
    {
        
        if (Input.GetMouseButtonDown(0))
        {
            
            Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if(Physics.Raycast(ray, out hit) == true && hit.collider.CompareTag("Floating") && floating == false)
            {
                MeshRenderer _mr = hit.collider.gameObject.GetComponent<MeshRenderer>();
                _mr.material.color = Color.blue;
                hit.rigidbody.useGravity = false;
                hit.rigidbody.AddForce(Vector3.up * 25, ForceMode.Impulse);
                floating = true;

            }else if(Physics.Raycast(ray, out hit) == true && hit.collider.CompareTag("Floating") && floating == true)
            {
                MeshRenderer _mr = hit.collider.gameObject.GetComponent<MeshRenderer>();
                _mr.material.color = Color.red + Color.yellow;
                hit.rigidbody.useGravity = true;
                hit.rigidbody.AddForce(Vector3.up * 2, ForceMode.Impulse);
                floating = false;
            }
        }

        

    }

   

}
