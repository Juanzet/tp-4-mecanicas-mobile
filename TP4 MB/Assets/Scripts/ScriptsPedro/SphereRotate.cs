﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereRotate : MonoBehaviour
{

    public float RotationSpeed;

    Rigidbody _rigidbody;

    public MeshRenderer _mesh;

    public static bool gameOver = false;

    public static int Points;



    void Start()
    {
        gameOver = false;
        Points = 0;

    }

    
    void Update()
    {
        if(GameManager.pause == false)
        {
            transform.Rotate(Vector3.forward * RotationSpeed * Time.deltaTime);

        }

        if(GameManager.stage == 0)
        {
            if (Points == 5)
            {
                _mesh.material.color = Color.green;
            }

            if (Points == 10)
            {
                _mesh.material.color = Color.yellow;
            }

            if (Points == 15)
            {
                _mesh.material.color = Color.red;

            }
        }

        if(GameManager.stage == 1)
        {
            for(int i = 0; i <1; i++)
            {
                State();
            }

            if (Points == 5)
            {
                _mesh.material.color = Color.green;
            }

            if (Points == 10)
            {
                _mesh.material.color = Color.yellow;
            }

            if (Points == 15)
            {
                _mesh.material.color = Color.red + Color.yellow;

            }

            if (Points == 20)
            {
                _mesh.material.color = Color.magenta;

            }

            if (Points == 25)
            {
                _mesh.material.color = Color.red;

            }
        }

        if (GameManager.stage == 2)
        {
            for (int i = 0; i < 1; i++)
            {
                State();
            }

            if (Points == 5)
            {
                _mesh.material.color = Color.green;
            }

            if (Points == 10)
            {
                _mesh.material.color = Color.yellow;
            }

            if (Points == 15)
            {
                _mesh.material.color = Color.red + Color.yellow;

            }

            if (Points == 20)
            {
                _mesh.material.color = Color.magenta;

            }

            if (Points == 25)
            {
                _mesh.material.color = Color.red + Color.white;

            }

            if (Points == 30)
            {
                _mesh.material.color = Color.red;

            }



        }

    }

  private void State()
    {
        switch (GameManager.stage)
        {
            case 1:
                Destroy(transform.Find("Cube"));
                Points = 0;
                _mesh.material.color = Color.blue;
                break;
            case 2:
                Destroy(transform.Find("Cube"));
                Points = 0;
                _mesh.material.color = Color.blue;
                break;
            default:
                _mesh.material.color = Color.blue;
                break;
        }
    }


   

    public bool GameOver(bool pGameOver)
    {
        gameOver = pGameOver;
        return gameOver;
    }

}
