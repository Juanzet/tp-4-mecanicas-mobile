﻿using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] 
    private GameObject[] levelPart;
    [SerializeField]
    private float minimumDistance;
    [SerializeField]
    private Transform lastPoint;
    [SerializeField]
    private int initialAmount;

    private Transform player;


    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;

        for (int i = 0; i < initialAmount; i++)
        {
            GeneratePartlevel();
        }
    }

    void Update()
    {
        if (Vector2.Distance(player.position, lastPoint.position)<minimumDistance)
        {
            GeneratePartlevel();
        }
            
    }

    private void GeneratePartlevel()
    {
        int randomNumber = Random.Range(0, levelPart.Length);
        GameObject level = Instantiate(levelPart[randomNumber], lastPoint.position, Quaternion.identity);
        lastPoint = SeekEndPoint(level, "PuntoFinal");
    }

    private Transform SeekEndPoint(GameObject levelPart, string tag)
    {
        Transform point = null;

        foreach (Transform location in levelPart.transform)
        {
            if (location.CompareTag(tag))
            {
                point = location;
                break;
            }
        }

        return point;

    }
   
}
