﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereScript : MonoBehaviour
{
    public float velocityMovement;
    void Update()
    {
        transform.position += Vector3.right * velocityMovement * Time.deltaTime;
    }
}
