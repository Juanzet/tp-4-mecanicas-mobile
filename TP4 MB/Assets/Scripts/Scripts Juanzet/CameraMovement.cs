﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{

    public float cameraVelocity;
    void Update()
    {
        transform.position += Vector3.down * cameraVelocity * Time.deltaTime;

        if(PlayerControler.powers == 1)
        {
            cameraVelocity += 1.5f;

            PlayerControler.powers = 0;

        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            Destroy(collision.gameObject);
        }
    }

    
}
