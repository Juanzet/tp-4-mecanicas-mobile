﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTablon : MonoBehaviour
{
    
    public float forceRotation;

    void Update()
    {
        LogicTable();
    }

    private void LogicTable()
    {
        if (Input.GetKey(KeyCode.D))
        {
            forceRotation++;
            transform.rotation = Quaternion.Euler(Vector3.forward * forceRotation);

        }

        if (Input.GetKey(KeyCode.A))
        {
            forceRotation--;
            transform.rotation = Quaternion.Euler(Vector3.forward * forceRotation);

        }
    }
}
