﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class ColliderManager : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            SceneManager.LoadScene("Mecanica31");
        }
    }
}
