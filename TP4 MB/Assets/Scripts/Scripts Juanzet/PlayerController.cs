﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Rigidbody rb;
    public float jumpForce;
    public float velocityMovement;
    public Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }
    void Update()
    {
        if (Input.anyKeyDown)
        {
            rb.velocity = Vector3.up * jumpForce;
            anim.SetBool("isFlying", true);
        }
        transform.position += Vector3.forward * velocityMovement * Time.deltaTime;
        anim.SetBool("isFlying", false);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Enemy"))
        {
            SceneManager.LoadScene("Mecanica7");
        }
    }
}
